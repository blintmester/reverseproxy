package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/google/logger"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type Domain struct {
	Dom     string
	Handler http.Handler
}

func DomainRouter(domains []Domain) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		for _, d := range domains {
			if request.Host == d.Dom {
				d.Handler.ServeHTTP(writer, request)
			}
		}
	})
}

func CreateProxy(ur string) http.Handler {
	u, _ := url.Parse(ur)
	proxy := httputil.NewSingleHostReverseProxy(u)
	return proxy
}

func main() {

	/*domainhandler:= DomainRouter([]Domain{
		{
			Dom:     "rethelyi.space",
			Handler: CreateProxy("https://v6.rethelyi.space"),
		},
	})*/

	httpServer := &http.Server{
		Handler: CreateProxy("https://v6.rethelyi.space"),
		Addr:    ":80",
	}

	validUrl := []string{
		"blintserver.sch.bme.hu", "blint.sch.bme.hu", "rethelyi.space", "v6.rethelyi.space", "sch.rethelyi.space", "home.rethelyi.space"}
	hostPolicy := func(_ context.Context, host string) error {
		for _, u := range validUrl {
			cut := len(host) - len(u)
			if cut >= 0 {
				chost := host[cut:]
				if chost == u {
					return nil
				}
			}
		}
		return fmt.Errorf("acme/autocert: host %q not configured in HostWhitelist", host)
	}
	m := &autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		HostPolicy: hostPolicy,
		Cache:      autocert.DirCache("keys"),
	}
	httpServer.Handler = m.HTTPHandler(nil)
	httpsServer := &http.Server{
		Handler:   CreateProxy("https://v6.rethelyi.space"),
		Addr:      ":443",
		TLSConfig: &tls.Config{GetCertificate: m.GetCertificate},
	}
	go func() {
		for true {
			err := httpsServer.ListenAndServeTLS("", "")
			logger.Errorf("%v", err)
		}
	}()

	for true {
		err := httpServer.ListenAndServe()
		logger.Errorf("%v", err)
	}
}
